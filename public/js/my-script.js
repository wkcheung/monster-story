var memoryMatrix = {};

const INTERVAL_WRITE_TO_MEMORY = 1000;
const TOTAL_MONSTER_COUNT = 3;

var windowHeight = $(window).height();
var windowWidth = $(window).width();

var MSECONDS_TO_WIN = 60000;
var INTERVAL_UPDATE_MONSTER_STATUS = 1000;
var timeToShowMonster = 1000;
var timeToHideMonster = 5000;

var showMonsterTimeout = null;
var hideMonsterTimeout = null;
var changeGameAreaTimeout = null;

var checkWinInterval = null;
var decreaseMonsterStatusInterval = null;

var wildMonsters = ['drogon', 'golem', 'skeleton'];

var msecondsLeftToWin = MSECONDS_TO_WIN;

var isFeedInstructionShown = false;
var isCaughtMonster = false;
var isJumping = false;
var modalLockSeconds = 0;
var modalTimeoutQueue = [];

var startMusic = null;
var themeMusic = null;
var deadMusic = null;

function writeToMemory() {
    localStorage.setItem("memory", JSON.stringify(memoryMatrix));
}

function writeToMemory(key, value) {
    memoryMatrix[key] = value;
    localStorage.setItem("memory", JSON.stringify(memoryMatrix));
}

function resetMemory() {
    memoryMatrix = {};
    localStorage.removeItem("memory");
    $("#continue-play-container").hide();
    $("#first-play-container").show();
}

function start() {
    var heroName = $("#hero-name-input").val();
    if (heroName) {
        writeToMemory("heroName", heroName);
        $(".hero-name-text").text(memoryMatrix["heroName"]);
        $(".game-view").fadeOut("slow");
        $("#game-instruction").fadeIn("slow");
    }
}

function showModal(modalSelector, mseconds=3000, htmlContent) {
    if (modalLockSeconds == 0) {
        modalLockSeconds = mseconds;
        if (htmlContent) {
            $(modalSelector + " .modal-content .modal-body").html(htmlContent);
        }
        $(modalSelector).slideDown();
        setTimeout(function() {
            $(modalSelector).slideUp();
            modalLockSeconds = 0;
        }, mseconds);
    } else {
        clearModalTimeoutQueue();
        modalTimeoutQueue.push(setTimeout(function() {
            showModal(modalSelector, mseconds, htmlContent);
        }, modalLockSeconds));
    }
}

function clearModalTimeoutQueue() {
    for (var k in modalTimeoutQueue) {
        clearTimeout(modalTimeoutQueue[k]);
    }
}

function changeGameArea(pageId) {
    if (changeGameAreaTimeout) {
        clearTimeout(changeGameAreaTimeout);
        changeGameAreaTimeout = null;
    }
    clearModalTimeoutQueue();

    $(".sidebar .rpgui-button.golden").removeClass("down");
    $("." + pageId + "-button").addClass("down");
    $(".game-area").hide();
    $("#" + pageId).fadeIn();
    if (pageId == "home-game-area") {
        if (!memoryMatrix.monsters || Object.keys(memoryMatrix.monsters).length == 0) {
            showModal(".no-monster-modal", 2000);
        } else if (memoryMatrix.monsters) {
            if (!isFeedInstructionShown) {
                showModal("#feed-monster-modal", 8000);
                isFeedInstructionShown = true;
            } else {
                var aliveCaughtMonsterCount = 0;
                for (var monsterType in memoryMatrix.monsters) {
                    if (memoryMatrix.monsters[monsterType].isAlive) {
                        aliveCaughtMonsterCount++;
                    }
                }
                if (aliveCaughtMonsterCount == TOTAL_MONSTER_COUNT) {
                    showModal("#three-monster-modal");
                }
            }
        }
    } else if (pageId == "catch-game-area") {
        showModal("#catch-monster-modal", 5000);
    }
}

function refreshHearts(heartCount) {
    $("#heart-container").html("");
    for (var i = 0; i < 5; i++) {
        if (i < heartCount) {
            $("#heart-container").append("<span class='heart'></span>");
        } else {
            $("#heart-container").append("<span class='empty-heart'></span>");
        }
    }
}

function isWon() {
    if ($("#heart-container").is(":hidden")) {
        $("#heart-container").fadeIn();
    }
    if (msecondsLeftToWin == 0 || memoryMatrix.isWon) {
        clearInterval(decreaseMonsterStatusInterval);
        $(".win-message").show();
        memoryMatrix.isWon = true;
        refreshHearts(5);
        return true;
    } else {
        if (msecondsLeftToWin == MSECONDS_TO_WIN) {
            refreshHearts(0);
        } else if (msecondsLeftToWin == 50000) {
            showModal("#home-universal-modal", 5000, "They look at you angrily.<br>You hope you wouldn't be eaten alive.");
            refreshHearts(1);
        } else if (msecondsLeftToWin == 40000) {
            showModal("#home-universal-modal", 5000, "Now it's better. They don't look at you at all.");
        } else if (msecondsLeftToWin == 30000) {
            showModal("#home-universal-modal", 5000, "They are more used to the human smell now.");
            refreshHearts(2);
        } else if (msecondsLeftToWin == 20000) {
            showModal("#home-universal-modal", 5000, "They even start to like you now!");
            refreshHearts(3);
        } else if (msecondsLeftToWin == 10000) {
            showModal("#home-universal-modal", 3000, "Watching TV with you is their newest hobby.");
            refreshHearts(4);
        } else if (msecondsLeftToWin == 5000) {
            showModal("#home-universal-modal", 3000, "You feel like you have a connection with these creatures.");
        }
        msecondsLeftToWin-=1000;
        return false;
    }
}

function setLevel(level) {
    if (level == 'easy') {

    } else if (level == 'difficult') {
        INTERVAL_UPDATE_MONSTER_STATUS = 700;
        timeToShowMonster = 1000;
        timeToHideMonster = 3000;
    }
}

function play(level='easy') {
    console.log("Let's play!")

    if (memoryMatrix.level) {
        level = memoryMatrix.level;
    } else {
        memoryMatrix.level = level;
    }
    setLevel(level);

    $(".game-view").fadeOut("slow");
    $("#main-game").fadeIn("slow");

    setInterval(writeToMemory, INTERVAL_WRITE_TO_MEMORY);

    initHomeGameArea();

    if (!memoryMatrix.monsters || Object.keys(memoryMatrix.monsters).length == 0) {
        showModal(".no-monster-modal", 2000);
    }

    if (!isWon()) {
        for (var monsterType in memoryMatrix.monsters) {
            if (memoryMatrix.monsters[monsterType].isAlive) {
                removeWildMonster(monsterType);
            }
        }

        decreaseMonsterStatusInterval = setInterval(decreaseMonsterStatus, INTERVAL_UPDATE_MONSTER_STATUS);
        
        requestAnimationFrame(check);
        
        showMonsterTimeout = setTimeout(showMonster, 1000);
        $(".pokeball").css("animation-play-state", "running");
        $(document).on("keydown", function (e) {
            if (e.keyCode == 32 && $("#catch-game-area").is(":visible"))
                jump();
        });
        $("#row").on("animationiteration", function () {
            $("#row").css("animation-play-state", "paused");
            $(".pokeball").css("animation-play-state", "running");
            isJumping = false;
        });
    }
    

    var aliveCaughtMonsterCount = 0;
    for (var monsterType in memoryMatrix.monsters) {
        if (memoryMatrix.monsters[monsterType].isAlive) {
            aliveCaughtMonsterCount++;
        }
    }
    if (aliveCaughtMonsterCount < TOTAL_MONSTER_COUNT) {
        changeGameArea('catch-game-area');
    }
}

function decreaseMonsterStatus() {
    var aliveCaughtMonsterCount = 0;
    $(".home-monster").each(function (index, element) {
        var monsterType = $(element).attr('id').replace('home-', '');
        if (memoryMatrix.monsters[monsterType].isAlive) {
            if (memoryMatrix.monsters[monsterType].health > 0) {
                memoryMatrix.monsters[monsterType].health -= 0.01;
                memoryMatrix.monsters[monsterType].health = Math.round(memoryMatrix.monsters[monsterType].health * 100) / 100;
            } else {
                memoryMatrix.monsters[monsterType].isAlive = false;
            }
            if (memoryMatrix.monsters[monsterType].mana > 0) {
                memoryMatrix.monsters[monsterType].mana -= 0.03;
                memoryMatrix.monsters[monsterType].mana = Math.round(memoryMatrix.monsters[monsterType].mana * 100) / 100;
            } else {
                memoryMatrix.monsters[monsterType].isAlive = false;
            }
            if (memoryMatrix.monsters[monsterType].stamina > 0) {
                memoryMatrix.monsters[monsterType].stamina -= 0.02;
                memoryMatrix.monsters[monsterType].stamina = Math.round(memoryMatrix.monsters[monsterType].stamina * 100) / 100;
            } else {
                memoryMatrix.monsters[monsterType].isAlive = false;
            }

            if (!memoryMatrix.monsters[monsterType].isAlive) {
                memoryMatrix.monsters[monsterType].health = memoryMatrix.monsters[monsterType].mana = memoryMatrix.monsters[monsterType].stamina = 0;
                $(element).removeClass(monsterType + "-idle");
                $(element).addClass(monsterType + "-dead");
                $(element).css("animation-play-state", "running");
                
                wildMonsters.push(monsterType);

                if (wildMonsters.length == TOTAL_MONSTER_COUNT) {
                    clearModalTimeoutQueue();
                    showModal("#all-dead-monster-modal", 2000);
                    changeGameAreaTimeout = setTimeout(function () {
                        changeGameArea("catch-game-area");
                    }, 5000);
                } else {
                    showModal("#dead-monster-modal");
                }
            } else {
                aliveCaughtMonsterCount++;
            }
        }
    });

    if (aliveCaughtMonsterCount == TOTAL_MONSTER_COUNT && checkWinInterval == null) {
        $("#game-hint").removeClass("golden");
        $("#game-hint").html("Keep them all alive to tame!");
        checkWinInterval = setInterval(isWon, 1000);
    } else if (aliveCaughtMonsterCount < TOTAL_MONSTER_COUNT) {
        msecondsLeftToWin = MSECONDS_TO_WIN;
        clearInterval(checkWinInterval);
        checkWinInterval = null;
        $("#game-hint").addClass("golden");
        $("#game-hint").text("Catch all three monsters now!");
        $("#heart-container").hide();
    }
}

function initHomeGameArea() {
    var i = 0;
    for (monsterType in memoryMatrix.monsters) {
        if (memoryMatrix.monsters[monsterType].isAlive) {
            $("#home-game-area .rpgui-icon").eq(i).html('<div id="home-' + monsterType + '" class="home-monster ' + monsterType + '-idle"></div>');
        } else {
            $("#home-game-area .rpgui-icon").eq(i).html('<div id="home-' + monsterType + '" class="home-monster ' + monsterType + '-dead"></div>');
            $("#home-game-area .rpgui-icon #home-" + monsterType).css("animation-play-state", "running");
        }
        i++;
    }

    $(".potion").draggable();

    $(".home-monster").droppable({
        drop: function (event, ui) {
            $(ui.draggable).attr("style", "position: relative;")
            monsterType = $(this).attr('id').replace('home-', '');
            if (memoryMatrix.monsters[monsterType].isAlive) {
                if ($(ui.draggable).hasClass("potion-red")) {
                    if (memoryMatrix.monsters[monsterType].health < 0.8) {
                        memoryMatrix.monsters[monsterType].health += 0.2;
                    }
                } else if ($(ui.draggable).hasClass("potion-blue")) {
                    if (memoryMatrix.monsters[monsterType].mana < 0.8) {
                        memoryMatrix.monsters[monsterType].mana += 0.2;
                    }
                } else if ($(ui.draggable).hasClass("potion-green")) {
                    if (memoryMatrix.monsters[monsterType].stamina < 0.8) {
                        memoryMatrix.monsters[monsterType].stamina += 0.2;
                    }
                }
            }
        }
    });

    var refreshStatusBarsInterval = null;
    $(".home-monster").hover(function () {
        var monsterType = $(this).attr('id').replace('home-', '');
        if ($("#home-monster-status").is(":hidden") || $("#monster-name").text() != monsterType) {
            $("#monster-name").hide();
            $("#home-monster-status").hide();
            $("#monster-name").text(monsterType);

            // Refresh UI status bars
            function refreshStatusBars() {
                var monsterType = $("#monster-name").text();
                if (memoryMatrix.monsters[monsterType].isAlive) {
                    RPGUI.set_value(document.getElementById("health-bar"), memoryMatrix.monsters[monsterType].health.toString());
                    RPGUI.set_value(document.getElementById("mana-bar"), memoryMatrix.monsters[monsterType].mana.toString());
                    RPGUI.set_value(document.getElementById("stamina-bar"), memoryMatrix.monsters[monsterType].stamina.toString());
                } else {
                    RPGUI.set_value(document.getElementById("health-bar"), "0");
                    RPGUI.set_value(document.getElementById("mana-bar"), "0");
                    RPGUI.set_value(document.getElementById("stamina-bar"), "0");
                }
            }
            refreshStatusBars();
            if (memoryMatrix.monsters[monsterType].isAlive) {
                if (refreshStatusBarsInterval != null) {
                    clearInterval(refreshStatusBarsInterval);
                }
                refreshStatusBarsInterval = setInterval(refreshStatusBars, INTERVAL_UPDATE_MONSTER_STATUS);
            }

            $("#monster-name").slideDown();
            $("#home-monster-status").slideDown();
        }
    }, function () { });
}

function hideMonster() {
    $(".wild-monstern").hide();
    setTimeout(showMonster, showMonsterTimeout);
}

function showMonster() {
    for (var i = 0; i < wildMonsters.length; i++) {
        var monsterType = wildMonsters[i];
        var randposx = Math.floor(Math.random() * (windowWidth*0.7));
        var randposy = Math.floor(Math.random() * (i * 100 + windowHeight/3));
        $("#raw-" + monsterType).css("left", randposx);
        $("#raw-" + monsterType).css("top", randposy);
        $("#raw-" + monsterType).show();
    }
    hideMonsterTimeout = setTimeout(hideMonster, timeToHideMonster);
}

function jump() {
    if (!isJumping) {
        isJumping = true;
        $("#row").css("animation-play-state", "running");
        $(".pokeball").css("animation-play-state", "paused"); 
    }
}

function removeWildMonster(monsterType) {
    var index = wildMonsters.indexOf(monsterType);
    if (index > -1) {
        wildMonsters.splice(index, 1);
    }
}

function addMonsterToMemory(monsterType) {
    if (!memoryMatrix.monsters) {
        memoryMatrix.monsters = {};
    }
    memoryMatrix.monsters[monsterType] = { health: 1.0, mana: 1.0, stamina: 1.0, isAlive: true };
    initHomeGameArea();
}

function check() {
    var pokeballMatrix = $(".pokeball").css("transform").replace(/[^0-9\-.,]/g, '').split(",");
    var x = parseFloat(pokeballMatrix[4]);
    var y = parseFloat(pokeballMatrix[5]) + parseFloat($("#row").css("transform").replace(/[^0-9\-.,]/g, '').split(",")[5]);
    
    for (var i = 0; i < wildMonsters.length; i++) {
        var monsterType = wildMonsters[i];
        if (!(x > $("#raw-" + monsterType).position().left + 100 || x + 100 < $("#raw-" + monsterType).position().left)
            && y < $("#raw-" + monsterType).position().top + 50) {
                if (!isCaughtMonster) {
                    isCaughtMonster = true;

                    if (wildMonsters.length == 1) {
                        // Catching the last wild monster
                        showModal("#caught-all-monster-modal");
                        changeGameAreaTimeout = setTimeout(function () {
                            changeGameArea('home-game-area');
                        }, 5000);
                    } else {
                        showModal("#caught-monster-modal", 2000);
                    }
        
                    $("#raw-" + monsterType).fadeOut();
                    $("#row").css("animation-play-state", "paused");
                    $(".pokeball").fadeOut(400, function() {
                        $("#row").css("animation-play-state", "running");
                    });
        
                    removeWildMonster(monsterType);
                    addMonsterToMemory(monsterType);
        
                    $(".pokeball").css("transform", "translateY(85vh)");

                    setTimeout(function () {
                        $(".pokeball").fadeIn();
                        isCaughtMonster = false;
                    }, 2000);
                }
        }
    }
    requestAnimationFrame(check);
}

$(document).ready(function () {
    if (localStorage.getItem("memory")) {
        memoryMatrix = JSON.parse(localStorage.getItem("memory"));
        $(".hero-name-text").text(memoryMatrix["heroName"]);
        $("#continue-play-container").show();
    } else {
        $("#first-play-container").show();
    }
});

